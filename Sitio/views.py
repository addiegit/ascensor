from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

# Importando los modelos
from .models import Cliente, Tecnico, Ascensor, Orden, Ciudad, Comuna, Asignacion

# Cosas para la autentificación
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required

# Templates generales
@login_required(login_url='login')
def index(request):
    user = request.session.get('user',None)
    return render(request,'Index.html',{'user':user})

def login(request):
    return render(request,'Login.html',{})

def loginIniciar(request):
    email = request.POST.get('email',False)
    password = request.POST.get('password',False)
    user = authenticate(request, username=email, password=password)
    if user is not None:
        request.session['user'] = user.id, user.nombre
        auth_login(request, user)
        return redirect('index')
    else:
        return redirect("login")

@login_required(login_url='login')
def cerrarSession(request):
    del request.session['user']
    logout(request)
    return redirect('index')

# Templates del panel de administración
@login_required(login_url='login')
def panel(request):
    user = request.session.get('user',None)
    return render(request,'Panel.html',{'user':user})

# Templates Asignación
@login_required(login_url='login')
def agregarAsignacion(request):
    user = request.session.get('user',None)
    clientes = Cliente.objects.all()
    tecnicos = Tecnico.objects.all()
    return render(request,'CrearAsignacion.html',{'user':user,'clientes':clientes,'tecnicos':tecnicos})

@login_required(login_url='login')
def crearAsignacion(request):
    idCliente = request.POST.get('idCliente',False)
    idTecnico = request.POST.get('idTecnico',False)
    asignacion = Asignacion(idCliente=idCliente, idTecnico=idTecnico, estado=0)
    asignacion.save()
    return redirect('panel')

@login_required(login_url='login')
def listarAsignacion(request):
    user = request.session.get('user',None)
    clientes = Cliente.objects.all()
    asignaciones = Asignacion.objects.all()
    return render(request,'ListarAsignacion.html',{'user':user,'clientes':clientes,'asignaciones':asignaciones})

@login_required(login_url='login')
def tomarAsignacion(request,idAsignacion,idCliente,idTecnico):
    user = request.session.get('user',None)

    asignacion = Asignacion.objects.get(pk = idAsignacion)    
    asignacion.estado = 1
    asignacion.save()
    
    clientes = Cliente.objects.get(pk = idCliente)
    tecnicos = Tecnico.objects.get(pk = idTecnico)
    ascensores = Ascensor.objects.all()

    return render(request,'CrearOrden.html',{'user':user,'cliente':clientes,'tecnico':tecnicos, 'ascensores':ascensores})

# Templates CRUD Ascensores
@login_required(login_url='login')
def agregarAscensor(request):
    user = request.session.get('user',None)
    return render(request,'CrearAscensor.html',{'user':user})

@login_required(login_url='login')
def crearAscensor(request):
    identificador = request.POST.get('identificador',False)
    modelo = request.POST.get('modelo',False)

    ascensor = Ascensor(identificador=identificador, modelo=modelo)
    
    ascensor.save()
    return redirect("listarAscensor")

@login_required(login_url='login')
def listarAscensor(request):
    user = request.session.get('user',None)
    ascensores = Ascensor.objects.all()
    return render(request,'ListarAscensor.html',{'user':user,'ascensores':ascensores})

@login_required(login_url='login')
def editarAscensor(request,id):
    user = request.session.get('user',None)
    ascensor = Ascensor.objects.get(pk = id)

    if request.POST.get('id'):
        id = request.POST.get('id',0)
        ascensor = Ascensor.objects.get(pk = id)

        identificador = request.POST.get('identificador',False)
        modelo = request.POST.get('modelo',False)
        
        ascensor.identificador = identificador
        ascensor.modelo = modelo
        ascensor.save()
        return redirect('listarAscensor')

    return render(request,'EditarAscensor.html',{'user':user,'ascensor':ascensor})

@login_required(login_url='login')
def borrarAscensor(request,id):
    ascensor = Ascensor.objects.get(pk = id)
    ascensor.delete()
    return redirect('listarAscensor')

# Templates CRUD Clientes
@login_required(login_url='login')
def registroCliente(request):
    user = request.session.get('user',None)
    ciudades = Ciudad.objects.all()
    comunas = Comuna.objects.all()
    return render(request,'CrearCliente.html',{'user':user,'ciudades':ciudades,'comunas':comunas})

@login_required(login_url='login')
def crearCliente(request):
    nombre = request.POST.get('nombre',False)
    direccion = request.POST.get('direccion',False)
    idCiudad = request.POST.get('idCiudad',False)
    idComuna = request.POST.get('idComuna',False)
    telefono = request.POST.get('telefono',False)
    email = request.POST.get('email',False)

    cliente = Cliente(nombre=nombre, direccion=direccion, idCiudad=idCiudad, idComuna=idComuna,
    telefono=telefono, email=email)
    
    cliente.save()
    return redirect("listarCliente")

@login_required(login_url='login')
def listarCliente(request):
    user = request.session.get('user',None)
    clientes = Cliente.objects.all()
    return render(request,'ListarCliente.html',{'user':user,'clientes':clientes})

@login_required(login_url='login')
def editarCliente(request,id):
    user = request.session.get('user',None)
    ciudades = Ciudad.objects.all()
    comunas = Comuna.objects.all()
    cliente = Cliente.objects.get(pk = id)

    if request.POST.get('id'):
        id = request.POST.get('id',0)
        cliente = Cliente.objects.get(pk = id)

        nombre = request.POST.get('nombre',False)
        direccion = request.POST.get('direccion',False)
        idCiudad = request.POST.get('idCiudad',False)
        idComuna = request.POST.get('idComuna',False)
        telefono = request.POST.get('telefono',False)
        email = request.POST.get('email',False)
        
        cliente.nombre = nombre
        cliente.direccion = direccion
        cliente.idCiudad = idCiudad
        cliente.idComuna = idComuna
        cliente.telefono = telefono
        cliente.email = email
        cliente.save()
        return redirect('listarCliente')

    return render(request,'EditarCliente.html',{'user':user,'ciudades':ciudades,'comunas':comunas,'cliente':cliente})

@login_required(login_url='login')
def borrarCliente(request,id):
    cliente = Cliente.objects.get(pk = id)
    cliente.delete()
    return redirect('listarCliente')

# Templates CRUD Ordenes
@login_required(login_url='login')
def generarOrden(request):
    user = request.session.get('user',None)
    clientes = Cliente.objects.all()
    tecnicos = Tecnico.objects.all()
    ascensores = Ascensor.objects.all()
    return render(request,'CrearOrden.html',{'user':user,'clientes':clientes,'tecnicos':tecnicos, 'ascensores':ascensores})

@login_required(login_url='login')
def crearOrden(request):
    idCliente = request.POST.get('idCliente',False)
    fechaOrden = request.POST.get('fechaOrden',False)
    horaInicio = request.POST.get('horaInicio',False)
    horaTermino = request.POST.get('horaTermino',False)
    idAscensor = request.POST.get('idAscensor',False)
    fallasEncontradas = request.POST.get('fallasEncontradas','')
    piezasRemplazadas = request.POST.get('piezasRemplazadas','')
    idTecnico = request.POST.get('idTecnico',False)

    ordenes = Orden(idCliente=idCliente, fechaOrden=fechaOrden, horaInicio=horaInicio, horaTermino=horaTermino,
    idAscensor=idAscensor, fallasEncontradas=fallasEncontradas, piezasRemplazadas=piezasRemplazadas, idTecnico=idTecnico)
    
    ordenes.save()
    return redirect("listarAsignacion")

@login_required(login_url='login')
def listarOrden(request):
    user = request.session.get('user',None)
    ordenes = Orden.objects.all()
    return render(request,'ListarOrden.html',{'user':user,'ordenes':ordenes})

@login_required(login_url='login')
def editarOrden(request,id):
    user = request.session.get('user',None)
    orden = Orden.objects.get(pk = id)
    clientes = Cliente.objects.all()
    tecnicos = Tecnico.objects.all()
    ascensores = Ascensor.objects.all()

    if request.POST.get('id'):
        id = request.POST.get('id',0)
        orden = Orden.objects.get(pk = id)

        idCliente = request.POST.get('idCliente',False)
        fechaOrden = request.POST.get('fechaOrden',False)
        horaInicio = request.POST.get('horaInicio',False)
        horaTermino = request.POST.get('horaTermino',False)
        idAscensor = request.POST.get('idAscensor',False)
        fallasEncontradas = request.POST.get('fallasEncontradas','')
        piezasRemplazadas = request.POST.get('piezasRemplazadas','')
        idTecnico = request.POST.get('idTecnico',False)
        
        orden.idCliente = idCliente
        orden.fechaOrden = fechaOrden
        orden.horaInicio = horaInicio
        orden.horaTermino = horaTermino
        orden.idAscensor = idAscensor
        orden.fallasEncontradas = fallasEncontradas
        orden.piezasRemplazadas = piezasRemplazadas
        orden.idTecnico = idTecnico
        orden.save()
        return redirect('listarOrden')

    return render(request,'EditarOrden.html',{'user':user,'clientes':clientes,'tecnicos':tecnicos, 'ascensores':ascensores,'orden':orden})

@login_required(login_url='login')
def borrarOrden(request,id):
    orden = Orden.objects.get(pk = id)
    orden.delete()
    return redirect('listarOrden')

# Templates CRUD Técnicos
def registroTecnico(request):
    user = request.session.get('user',None)
    ciudades = Ciudad.objects.all()
    comunas = Comuna.objects.all()
    return render(request,'CrearTecnico.html',{'user':user,'ciudades':ciudades,'comunas':comunas})

def crearTecnico(request):
    nombre = request.POST.get('nombre',False)
    direccion = request.POST.get('direccion',False)
    idCiudad = request.POST.get('idCiudad',False)
    idComuna = request.POST.get('idComuna',False)
    telefono = request.POST.get('telefono',False)
    email = request.POST.get('email',False)
    confirm_password = request.POST.get('confirm_password',False)

    tecnico = Tecnico(nombre=nombre, direccion=direccion, idCiudad=idCiudad, idComuna=idComuna,
    telefono=telefono, email=email, password=confirm_password)
    tecnico.set_password(confirm_password)

    tecnico.save()
    return redirect("index")

@login_required(login_url='login')
def listarTecnico(request):
    user = request.session.get('user',None)
    tecnicos = Tecnico.objects.all()
    return render(request,'ListarTecnico.html',{'user':user,'tecnicos':tecnicos})

@login_required(login_url='login')
def editarTecnico(request,id):
    user = request.session.get('user',None)
    ciudades = Ciudad.objects.all()
    comunas = Comuna.objects.all()
    tecnico = Tecnico.objects.get(pk = id)

    if request.POST.get('id'):
        id = request.POST.get('id',0)
        tecnico = Tecnico.objects.get(pk = id)

        nombre = request.POST.get('nombre',False)
        direccion = request.POST.get('direccion',False)
        idCiudad = request.POST.get('idCiudad',False)
        idComuna = request.POST.get('idComuna',False)
        telefono = request.POST.get('telefono',False)
        email = request.POST.get('email',False)
        confirm_password = request.POST.get('confirm_password',False)
        
        tecnico.nombre = nombre
        tecnico.direccion = direccion
        tecnico.idCiudad = idCiudad
        tecnico.idComuna = idComuna
        tecnico.telefono = telefono
        tecnico.email = email
        tecnico.confirm_password = confirm_password
        tecnico.save()
        return redirect('listarTecnico')

    return render(request,'EditarTecnico.html',{'user':user,'ciudades':ciudades,'comunas':comunas,'tecnico':tecnico})

@login_required(login_url='login')
def borrarTecnico(request,id):
    tecnico = Tecnico.objects.get(pk = id)
    tecnico.delete()
    return redirect('listarTecnico')