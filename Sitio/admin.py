from django.contrib import admin
from Sitio.models import Tecnico, Cliente, Ascensor, Orden, Ciudad, Comuna, Asignacion
from django.contrib.auth.admin import UserAdmin

# Asignación
class PersonalizadoAsignacion(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,idCliente,idTecnico,estado')
        })
    )

    list_display = ('id','idCliente','idTecnico','estado')
    search_fields = ('id',)
    ordering = ('id',)

# Ascensores
class PersonalizadoAscensor(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,identificador,modelo')
        })
    )

    list_display = ('id','identificador','modelo')
    search_fields = ('identificador',)
    ordering = ('id',)

# Ciudades
class PersonalizadoCiudad(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,descripcion')
        })
    )

    list_display = ('id','descripcion')
    search_fields = ('id',)
    ordering = ('id',)

# Clientes registrados
class PersonalizadoCliente(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,nombre,direccion,region,comuna,telefono,email')
        })
    )

    list_display = ('id','nombre','direccion','idCiudad','idComuna','telefono','email')
    search_fields = ('nombre',)
    ordering = ('id',)

# Comunas
class PersonalizadoComuna(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,descripcion,idCiudad')
        })
    )

    list_display = ('id','descripcion','idCiudad')
    search_fields = ('id',)
    ordering = ('id',)

# Órdenes
class PersonalizadoOrden(admin.ModelAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,idCliente,fechaOrden,horaInicio,horaTermino,idAscensor,fallasEncontradas,piezasRemplazadas,idTecnico')
        })
    )

    list_display = ('id','idCliente','fechaOrden','horaInicio','horaTermino','idAscensor','idTecnico')
    search_fields = ('id',)
    ordering = ('id',)

# Técnicos registrados
class PersonalizadoUserAdmin(UserAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None,{
            'fields':('id,email,password1,password2,nombre,direccion,region,comuna,telefono')
        })
    )

    list_display = ('id','nombre','direccion','idCiudad','idComuna','telefono','email','is_active','is_staff',)
    search_fields = ('nombre',)
    ordering = ('id',)

admin.site.register(Asignacion,PersonalizadoAsignacion)
admin.site.register(Ascensor,PersonalizadoAscensor)
admin.site.register(Ciudad,PersonalizadoCiudad)
admin.site.register(Cliente,PersonalizadoCliente)
admin.site.register(Comuna,PersonalizadoComuna)
admin.site.register(Orden,PersonalizadoOrden)
admin.site.register(Tecnico,PersonalizadoUserAdmin)