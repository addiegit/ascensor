// Tiempo
$.validator.addMethod('time24', function(value, element, param) {
    return value == '' || value.match(/^([01][0-9]|2[0-3]):[0-5][0-9]$/);
}, 'Hora inválida.');

// Selectores
$.validator.addMethod("selector", function(value, element, arg){
    return arg !== value;
}, "Seleccione una opción.");

// Requerimientos del formulario
$(function() {
    $( "#crearOrden" ).validate({
        rules: {
            idCliente:{
                required: true,
                selector: "empty"
            },
            idTecnico:{
                required: true,
                selector: "empty"
            },
            fechaOrden:{
                required: true,
                date: true
            },
            horaInicio:{
                required: true,
                time24: true
            },
            horaTermino:{
                required: true,
                time24: true
            },
            idAscensor:{
                required: true,
                selector: "empty"
            }
        },
        messages:{
            idCliente:{
                required: 'Selecciona un Cliente.',
            },
            idTecnico:{
                required: 'Selecciona un Técnico.'
            },
            fechaOrden:{
                required: 'Ingrese una fecha.',
                date: 'Ingrese una fecha válida.'
            },
            horaInicio:{
                required: 'Ingrese una hora.',
                time24: 'Ingrese una hora válida.'
            },
            horaTermino:{
                required: 'Ingrese una hora.',
                time24: 'Ingrese una hora válida.'   
            },
            idAscensor:{
                required: 'Selecciona un Ascensor.',
            }
        }
    });
 });