// Requerimientos del formulario
$(function() {
    $( "#crearAscensor" ).validate({
        rules: {
            identificador:{
                required: true,
            },
            modelo:{
                required: true,
            }
        },
        messages:{
            identificador:{
                required: "Ingresa el identificador del ascensor."
            },
            modelo:{
                required: "Ingresa el modelo del ascensor."
            },
        }
    });
 });